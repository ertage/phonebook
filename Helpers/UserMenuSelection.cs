using PhoneBook.Contacts;
using PhoneBook.Data;

namespace PhoneBook.Helpers;

public static class UserMenuSelection
{
    public static void UserMenu()
    {
        using var db = new ContactContext();
        const string menuHeader = "PHONE BOOK";
        var menu = true;
        while (menu)
        {
            var input = MenuPrompt.RunMenuPrompt(MenuOptions.FirstMenu, menuHeader);
            switch (input)
            {
                case 0:
                    Read.ReadContact(db);
                    break;
                case 1:
                    Create.CreateContact(db);
                    break;
                case 2:
                    Update.UpdateContact(db);
                    break;
                case 3:
                    Delete.DeleteContact(db);
                    break;
                case 4:
                    Environment.Exit(0);
                    break;
            }
        }
    }
}