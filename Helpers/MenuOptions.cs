namespace PhoneBook.Helpers;

public static class MenuOptions
{
    public static readonly List<string> FirstMenu = new()
    {
    "Show Contacts", //0
    "Add New Contact", //1
    "Edit Contact", //2
    "Delete Contact", //3
    "Exit" //4
    };
}