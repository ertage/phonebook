using System.ComponentModel.DataAnnotations;
using PhoneBook.Models;

namespace PhoneBook.Helpers;

public class InputValidationDB
{
    public static bool UserInPutValidation(List<string> newContact)
    {
        var item = new Contact()
        {
            ContactName = newContact[0],
            PhoneNumber = newContact[1]
        };
                var context = new ValidationContext(item, serviceProvider: null, items: null);
                var errorResults = new List<ValidationResult>();

                var isValid = Validator.TryValidateObject(item, context, errorResults);

                if (!Validator.TryValidateObject(item, context, errorResults, true)) {
                    foreach (var errorInformation in errorResults)
                    {
                        Console.WriteLine(errorInformation);
                        return false;
                    }
                }
                return true;
    }
}