using System.Text.RegularExpressions;
using PhoneBook.Data;

namespace PhoneBook.Helpers;

public static class ContactsMenuList
{
    public static int SelectContactId(string menuHeader)
    {
        using var db = new ContactContext(); 
        var contacts = db.Contacts.ToList();
        var contactList = new List<string>();

        foreach (var items in contacts)
        {
            contactList.Add("Name: " + items.ContactName + " " + "| Tel. " + items.PhoneNumber);
        }
        
        var selectionNumber = MenuPrompt.RunMenuPrompt(contactList, menuHeader);
        var selectedContactName = Regex.Match(contactList.ElementAt(selectionNumber),
            @"(?![Name: ])(.*?)(?=\s\| Tel.)").Value;
        var selectedPhoneNumber = Regex.Match(contactList.ElementAt(selectionNumber),
            @"\d+").Value;
        var selectionId = db.Contacts.FirstOrDefault(x =>
            x.ContactName == selectedContactName
            && x.PhoneNumber == selectedPhoneNumber);
         return selectionId.ContactID;
    }
}