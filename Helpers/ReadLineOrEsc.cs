namespace PhoneBook.Helpers;

// Modified Version of https://stackoverflow.com/a/18902318

public static class ReadLineOrEsc
{
    public static string ReadLineUntilEsc()
        {
            var retString = "";
            var currentPost = Console.GetCursorPosition();
            var fixPos = currentPost.Left;

            do
            {
                ConsoleKeyInfo readKeyResult = Console.ReadKey(true);

                if (readKeyResult.Key == ConsoleKey.Escape)
                {
                    Console.WriteLine();
                    return null;
                }

                if (readKeyResult.Key == ConsoleKey.Enter)
                {
                    Console.WriteLine();
                    return retString;
                }

                if (readKeyResult.Key == ConsoleKey.Backspace)
                {
                    if (currentPost.Left > fixPos)
                    {
                        retString = retString.Remove(retString.Length - 1);
                        Console.SetCursorPosition(currentPost.Left - 1, Console.CursorTop);
                        Console.Write(' ');
                        Console.SetCursorPosition(currentPost.Left - 1, Console.CursorTop);
                        currentPost.Left--;
                    }
                }
                else
                {
                    retString += readKeyResult.KeyChar;
                    Console.Write(readKeyResult.KeyChar);
                    currentPost.Left++;
                }
            }
            while (true);
        }
}