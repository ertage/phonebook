namespace PhoneBook.Helpers;

public static class NewContactDataInput
{
        public static List<string> NewContactData()
        {
                Console.Clear();
                List<string> newContact = new List<string>();
                bool result;
                do
                {
                    newContact.Clear();
                    Console.WriteLine("Press ESC to return to Main Menu");
                    Console.Write("Enter Full Name:");
                    string? contactName = ReadLineOrEsc.ReadLineUntilEsc();
                    if (contactName == null)
                    {
                        UserMenuSelection.UserMenu();
                    }
                    Console.Write("Enter PhoneNumber:");
                    string? phoneNumber = ReadLineOrEsc.ReadLineUntilEsc();
                    if (phoneNumber == null)
                    {
                        UserMenuSelection.UserMenu();
                    }
                    newContact.Add(contactName);
                    newContact.Add(phoneNumber);
                    result = InputValidationDB.UserInPutValidation(newContact);
                } while (result == false);
                return newContact;
        }
}