using System.Collections;

namespace PhoneBook.Helpers;

public static class MenuPrompt
{
    private static int CurrentSelectedItem { get; set; }
    private static int TopPos { get; set; }
    private static ConsoleKey PressedKey { get; set; }

    private static void MoveCursor(ICollection menuOptions)
    {
        PressedKey = Console.ReadKey().Key;
        if (PressedKey == ConsoleKey.DownArrow)
        {
            if (CurrentSelectedItem + 1 >= menuOptions.Count + TopPos)
            {
                CurrentSelectedItem = TopPos;
            }
            else
            {
                CurrentSelectedItem += 1;
            }
        }
        else if (PressedKey == ConsoleKey.UpArrow)
        {
            if (CurrentSelectedItem == TopPos)
            {
                CurrentSelectedItem = menuOptions.Count + TopPos - 1;
            }
            else
            {
                CurrentSelectedItem -= 1;
            }
        }
        else if (PressedKey == ConsoleKey.Escape)
        {
            Console.SetCursorPosition(0,0);
            UserMenuSelection.UserMenu();
        }
    }
    
    private static void DrawMenu(IEnumerable<string> menuOptions)
    {
        foreach (var item in menuOptions.Select((value, index) => new { index, value }))
        {
            if (CurrentSelectedItem - TopPos == item.index)
            {
                Console.SetCursorPosition(0, CurrentSelectedItem);
                Console.WriteLine("> " + item.value);
            }
            else
            {
                Console.SetCursorPosition(0, item.index + TopPos);
                Console.WriteLine("  " + item.value); 
            }
        }
    }
    
    
    public static int RunMenuPrompt(List<string> menuOptions, string menuHeader)
    {
        Console.Clear();
        Console.WriteLine(menuHeader);
        TopPos = Console.CursorTop;
        CurrentSelectedItem = Console.CursorTop;
        Console.CursorVisible = false;
        do
        {
            DrawMenu(menuOptions);
            MoveCursor(menuOptions);
        } while (PressedKey != ConsoleKey.Enter);

        return CurrentSelectedItem - TopPos;
    }
    
    public static int RunMenuPrompt(List<string> menuOptions)
    {
        Console.Clear();
        Console.CursorVisible = false;
        TopPos = Console.CursorTop;
        CurrentSelectedItem = Console.CursorTop;
        do
        {
            DrawMenu(menuOptions);
            MoveCursor(menuOptions);
        } while (PressedKey != ConsoleKey.Enter);

        return CurrentSelectedItem - TopPos;
    }

}