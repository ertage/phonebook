using System.ComponentModel.DataAnnotations;

namespace PhoneBook.Models;

public class Contact
{
    [Key]
    public int ContactID { get; set; }
    
    [Required]
    [MaxLength(50)]
    [RegularExpression("^[^\\s][a-zA-Z ]*$", ErrorMessage = "Only letters are allowed for the Name")]
    public string? ContactName { get; set; }
    
    [Required]
    [MaxLength(25)]
    [RegularExpression("^[^\\s][0-9]*$", ErrorMessage = "Only numbers are allowed for the Phone Number")]
    public string? PhoneNumber { get; set; }
}