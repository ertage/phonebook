using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PhoneBook.Models;

namespace PhoneBook.Data;
public class ContactContext : DbContext
{
    public DbSet<Contact> Contacts { get; set; }
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        IConfigurationRoot config = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json")
            .Build();
    
        optionsBuilder.UseSqlServer(config.GetConnectionString("MyCon"));
    }
}




