using PhoneBook.Data;
using PhoneBook.Helpers;

namespace PhoneBook.Contacts;

public static class Delete
{
    public static void DeleteContact(ContactContext db)
    {
        Console.Clear();
        string menuHeaderDeleteContact = @"Press ESC to return to Main Menu
Select Contact you want to delete:";
        if (!db.Contacts.Any())
        {
            Console.WriteLine("The Database is empty right now");
        }
        else
        {
            var selectedContactId = ContactsMenuList.SelectContactId(menuHeaderDeleteContact);
            var contactToDelete = db.Contacts.First(d => d.ContactID == selectedContactId);
            db.Contacts.Remove(contactToDelete);
            db.SaveChanges();
        }

        Console.WriteLine("");
        Console.WriteLine("Press any key to return");
        Console.ReadKey();

    }
}