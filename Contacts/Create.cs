using PhoneBook.Data;
using PhoneBook.Helpers;
using PhoneBook.Models;

namespace PhoneBook.Contacts;

public static class Create
{
    public static void CreateContact(ContactContext db)
    {
        var newContact = NewContactDataInput.NewContactData();
        db.Add(new Contact { ContactName = newContact[0], PhoneNumber = newContact[1]});
        db.SaveChanges();
    }

}