using PhoneBook.Data;

namespace PhoneBook.Contacts;

public static class Read
{
    public static void ReadContact(ContactContext db)
    {
        Console.Clear();
        Console.WriteLine("Contacts");
        if (!db.Contacts.Any())
        {
            Console.WriteLine("The Database is empty right now");
        }
        else
        {
            var contacts = db.Contacts.ToList();
            foreach (var items in contacts)
            {
                Console.WriteLine("Name: " + items.ContactName + " | Tel. " + items.PhoneNumber);
            }
        }
        Console.WriteLine("");
        Console.WriteLine("Press any key to return");
        Console.ReadKey();
    }
}