using PhoneBook.Data;
using PhoneBook.Helpers;

namespace PhoneBook.Contacts;

public static class Update
{
    public static void UpdateContact(ContactContext db)
    {
        Console.Clear();
        string menuHeaderUpdateContact = @"Press ESC to return to Main Menu
Select Contact you want to edit:";
        if (!db.Contacts.Any())
        {
            Console.WriteLine("The Database is empty right now");
        }
        else
        {
            var selectedContactId = ContactsMenuList.SelectContactId(menuHeaderUpdateContact);
            var contactToUpdate = NewContactDataInput.NewContactData();
            var result = db.Contacts.SingleOrDefault(b => b.ContactID == selectedContactId);
            result.ContactName = contactToUpdate[0];
            result.PhoneNumber = contactToUpdate[1];
            db.SaveChanges();
        }
        
        Console.WriteLine("");
        Console.WriteLine("Press any key to return");
        Console.ReadKey();
    }
}